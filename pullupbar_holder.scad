$fn = 100;

// dimensions in mm
ch = 7;
w = 100 - ch; // x
d = 110 - ch; // y
h = 10 - ch; // z

translate([-80,-h/2,0]){
minkowski(){
cube([30,h,h]);
resize([ch,ch,ch]){
rotate([45,-35,0]){
	cube(ch);
}}}
minkowski(){
rotate([0,45,0])cube([25,h,h]);
resize([ch,ch,ch]){
rotate([45,-35,0]){
	cube(ch);
}}}
}


difference(){
	difference(){
		minkowski(){
			linear_extrude(height = h) {
				polygon([
				[w/4,w/2] , [-w/4,w/2] , [-d/2,d/3] , [-d/2,-d/3] ,
				-[w/4,w/2] , -[-w/4,w/2] , -[-d/2,d/3] , -[-d/2,-d/3]
				]);
			}
			resize([ch,ch,ch]){
				rotate([45,-35,0]){
					cube(ch);
				}
			}
		}

		linear_extrude(height = 50) {
			intersection(){
				circle(37.5 + 1);
				square([100,50 + 2], center=true);
			}
		}
	}


	for (j=[0:99]) {
		hull(){
			for (i=[0:1]) {
				vec = rands(-70,70,2);
				translate(vec) sphere( 0.5 , $fn = 5);
			}
		}
	}
}
